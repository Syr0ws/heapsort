package fr.syrows;

import java.util.Arrays;
import java.util.Random;

public class Main {

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {

        // Example of use
        int n = 10_000;

        int[] array = new int[n];

        for(int i = 0; i < n; i++) array[i] = RANDOM.nextInt(n);

        System.out.println(Arrays.toString(array));

        ArraySorter.sort(array, array.length);

        System.out.println(Arrays.toString(array));
    }
}
