import fr.syrows.ArraySorter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

public class TestArraySorter {

    private static final Random RANDOM = new Random();

    private int[] array;

    @Before
    public void initArray() {

        int n = 10_000;

        this.array = new int[n];

        for(int i = 0; i < n; i++) array[i] = RANDOM.nextInt(n);
    }

    @Test
    public void testSorter() {

        int n = this.array.length;

        ArraySorter.sort(this.array, n);

        for(int i = 0; i < n; i++) {

            for(int j = i; j < n - 1; j++) {

                Assert.assertTrue(this.array[j] <= this.array[j + 1]);
            }
        }
    }
}
